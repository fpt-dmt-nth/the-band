import React, { Component } from 'react';
import '../Theband.css';
import Dropdown from 'react-bootstrap/dropdown'
import {
    Link
  } from "react-router-dom";
import SearchIcon from '@material-ui/icons/Search';
class Header extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
                    <div className="container">
                            <div className="d-flex justify-content-between w-100">
                                <ul className= "navbar-nav me-auto mb-2 mb-lg-0 ">
                                    <li className="navbar-brand">
                                        <Link to='/'>Logo</Link>
                                    </li>
                                </ul>
                                <ul className="navbar-nav me-auto mb-2 mb-lg-0 ">
                                    <li className="nav-item">
                                        <Link to='/' className="nav-link active" >Home</Link>
                                    </li>

                                    <li className="nav-item">
                                        <Link to='/band' className="nav-link">Band</Link>
                                    </li>

                                    <li className="nav-item">
                                        <Link to='/tour' className="nav-link" >Tour</Link>
                                    </li>

                                    <li className="nav-item">
                                        <Link to='/contact' className="nav-link" >Contact</Link>
                                    </li>
                                    <li className="nav-item">
                                    <Dropdown>
                                        <Dropdown.Toggle variant="dark" id="dropdown-basic">
                                            More
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu>
                                            <Dropdown.Item href="/merchandise">Merchandise</Dropdown.Item>
                                            <Dropdown.Item href="/extras">Extras</Dropdown.Item>
                                            <Dropdown.Item href="/media">Media</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    </li>
                                    <li className="d-flex">
                                        <button className="btn btn-dark ml-1" type="submit"><SearchIcon></SearchIcon></button>
                                    </li>
                                </ul>
                            </div>    
                        </div> 
                </nav>
            </div>
        );
    }
}

export default Header;