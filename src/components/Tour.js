import React, { Component } from 'react';
import '../Theband.css';
import paris from '../images/paris.jpg'
import newyork from '../images/newyork.jpg';
import sanfran from '../images/sanfran.jpg';

class Tour extends Component {
    render() {
        return (
            <div className='bg-1'>
                <div className='container mt-5 py-5'>
                    <h3 className='text-center'>TOUR DATES</h3>
                    <p className="text-center"><i>Lorem ipsum we'll play you some music.<br></br> Remember to book your tickets!</i></p>
                    <ul className="list-group">
                        <li className="list-group-item">September <span className="badge badge-danger">Sold Out!</span></li>
                        <li className="list-group-item">October <span className="badge badge-danger">Sold Out!</span></li>
                        <li className="list-group-item">November <span className="badge badge-secondary">3</span></li>
                    </ul>
                    <div className="row text-center mt-3">
                        <div className="col-sm-4">
                            <div className="img-thumbnail">
                                <img src={paris} alt="Paris" className="img-responsive fit-image"/>
                                <p><strong>Paris</strong></p>
                                <p>Fri. 27 November 2015</p>
                                <button className="btn btn-dark">Buy Tickets</button>
                            </div>
                        </div>
                        <div className="col-sm-4">
                            <div className="img-thumbnail">
                                <img src={newyork} alt="New York" className="img-responsive fit-image"/>
                                <p><strong>Paris</strong></p>
                                <p>Fri. 27 November 2015</p>
                                <button className="btn btn-dark">Buy Tickets</button>
                            </div>
                        </div>
                        <div className="col-sm-4">
                            <div className="img-thumbnail">
                                <img src={sanfran} alt="San Francisco" className="img-responsive fit-image"/>
                                <p><strong>Paris</strong></p>
                                <p>Fri. 27 November 2015</p>
                                <button className="btn btn-dark">Buy Tickets</button>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        );
    }
}

export default Tour;