import React, { Component } from 'react';
import bandpicture from '../images/atheleband.jpg';
import bandpicture2 from '../images/ny.jpg';
import bandpicture3 from '../images/la.jpg';
import Carousel from 'react-bootstrap/carousel';
import '../Theband.css';

class Slider extends Component {
    render() {
        return (
            
            <div className="bandpicture bg-1 py-3 my-5">
                <Carousel>
                    <Carousel.Item>
                        <img className="d-block w-100"
                        src={bandpicture}
                        alt="first slide"
                        />
                    <Carousel.Caption>
                        <h3>New York</h3>
                        <p>The atmosphere in New York is lorem ipsum.</p>
                    </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img className="d-block w-100"
                        src={bandpicture2}
                        alt="second slide"
                        />
                    <Carousel.Caption>
                        <h3>Chicago</h3>
                        <p>Thank you, Chicago - A night we won't forget.</p>
                    </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img className="d-block w-100"
                        src={bandpicture3}
                        alt="third slide"
                        />
                    <Carousel.Caption>
                        <h3>LA</h3>
                        <p>Even though the traffic was a mess, we had the best time playing at Venice Beach!</p>
                    </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            </div>    
        );
    }
}

export default Slider;