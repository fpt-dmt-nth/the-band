import React, { Component } from 'react';
import '../Theband.css';
import member1 from '../images/bandmember.jpg'

class Introduce extends Component {
    render() {
        return (
            <div className='container text-center py-5 my-5'>
                <div className='the_band_text'>
                    <h3>THE BAND</h3>
                    <p><i>We love music!</i></p>
                    <p>We have created a fictional band website. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
                <br></br>
                <div className='row'>
                    <div className="col-sm-4">
                        <p><strong>Duong Minh Tien</strong></p><br></br>
                        <img src={member1} alt="member 1" className="rounded-circle"/> 
                        <div id='info1' >
                            <p>Guitarist and Lead Vocalist</p>
                            <p>Loves long walks on the beach</p>
                            <p>Member since 1988</p>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <p><strong>Nguyen Thanh Hoan</strong></p><br></br>
                        <img src={member1} alt="member 2" className="rounded-circle"/>
                        <div id='info2' >
                            <p>Drummer</p>
                            <p>Loves drummin'</p>
                            <p>Member since 1988</p>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <p><strong>Ngo Minh Toan</strong></p><br></br>
                        <img src={member1} alt="member 3" className="rounded-circle"/>
                        <div id='info3' >
                            <p>Bass player</p>
                            <p>Loves math</p>
                            <p>Member since 2005</p>
                        </div>
                     </div>
                </div>    
            </div>
        );
    }
}

export default Introduce;