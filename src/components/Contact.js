import React, { Component } from 'react';
import { Tab, Tabs } from 'react-bootstrap';
import Tab1 from './Tab1'
import Tab2 from './Tab2';
import Tab3 from './Tab3';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import MailIcon from '@material-ui/icons/Mail';

class Contact extends Component {
    render() {
        return (
            <div className="container my-5 py-5" >
                <h3 className="text-center">Contact</h3>
                <p className="text-center"><i>We love our fans!</i></p>

                <div className="d-flex flex-column px-5 ">
                    <div className="col-md-4">
                        <p>Fan? Drop a note.</p>
                        <p><span><LocationOnIcon></LocationOnIcon></span>Chicago, US</p>
                        <p><span><PhoneIphoneIcon></PhoneIphoneIcon></span>Phone: +00 1515151515</p>
                        <p><span><MailIcon></MailIcon></span>Email: mail@mail.com</p>
                    </div>

                    <div className="col">
                        <div className="row">
                            <div className="col-sm-6 form-group">
                                <input className="form-control" id="name" name="name" placeholder="Name" type="text" required/>
                            </div>
                            <div className="col-sm-6 form-group">
                                <input className="form-control" id="email" name="email" placeholder="Email" type="email" required/>
                            </div>
                        </div>
                        <textarea className="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea>
                        <div className="row">
                            <div className="col-md-12 form-group">
                                <button className="btn btn-dark float-right mt-3" type="submit">Send</button>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 className='text-center'>From the blog</h3>
                <Tabs defaultActiveKey="tab1" id="uncontrolled-tab-example">
                    <Tab eventKey="tab1" title="Mike"><Tab1 />  
                    </Tab>
                    <Tab eventKey="tab2" title="Chandler"><Tab2 />
                    </Tab>
                    <Tab eventKey="tab3" title="Peter"><Tab3 />              
                    </Tab>
                </Tabs>
            </div>
        );
    }
}

export default Contact;