import React, { Component } from 'react';

class Tab2 extends Component {
    render() {
        return (
            <div>
                <h2>Chandler Bing, Guitarist</h2>
                <p>Always a pleasure people! Hope you enjoyed it as much as I did. Could I BE.. any more pleased? Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus lobortis a nulla eu convallis. Suspendisse ut eleifend nisi, eget convallis turpis. Etiam lacinia, est vel porta blandit, libero quam interdum odio, in tincidunt urna lectus at ex. Etiam felis ante, sollicitudin nec erat in, consectetur placerat lacus. Suspendisse tincidunt et turpis ac placerat. Cras dapibus libero id magna aliquam viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean pulvinar eu dolor id lacinia. Quisque nibh turpis, dignissim id rhoncus sed, accumsan vel lectus. Morbi laoreet, tellus vel varius porta, purus sem cursus erat, a scelerisque purus ex sed dui. Cras dolor nunc, imperdiet non nisl nec, consequat vulputate diam.</p>
            </div>
        );
    }
}

export default Tab2;