import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer className="bg-secondary text-center text-lg-start text-light">
                <div className="text-center p-3">
                    The Band - Made by Duong Minh Tien
                </div>    
            </footer>
        );
    }
}

export default Footer;