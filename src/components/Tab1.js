import React, { Component } from 'react';

class Tab1 extends Component {
    render() {
        return (
            <div>
                <h2>Mike Ross, Manager</h2>
                <p>Man, we've been on the road for some time now. Looking forward to lorem ipsum. Sed id eleifend quam, interdum lobortis tellus. Duis in accumsan ipsum. Pellentesque ultricies finibus urna, nec faucibus lectus iaculis vel. In hac habitasse platea dictumst. Suspendisse vitae justo mauris. Aenean tincidunt nibh ut metus imperdiet, a dictum lacus maximus. Maecenas eu leo lacinia, blandit ante pharetra, consequat ipsum. Nulla vitae elit tempus neque tempor tristique. Proin ac auctor odio. Phasellus vel arcu at felis pulvinar rhoncus quis ut lorem. Nulla in libero lacus. Mauris ac suscipit leo. Praesent nisl diam, consectetur quis rhoncus a, tincidunt sed mauris. Nulla varius velit at cursus aliquet. Nunc nunc odio, dictum quis egestas ut, molestie ut lorem.</p>
            </div>
        );
    }
}

export default Tab1;