import Header from './components/Header';
import Slider from './components/Slider';
import Introduce from './components/Introduce';
import Tour from './components/Tour';
import Contact from './components/Contact';
import Footer from './components/Footer';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <div>
        <Header />
        <Switch>
        <Route exact path='/'><Slider /><Introduce /><Tour /><Contact /></Route> 
        </Switch>
        <Switch>
          <Route path='/band' component={Introduce}/>
        </Switch>
        <Switch>
          <Route path='/tour' component={Tour}/>
        </Switch>
        <Switch>
          <Route path='/contact' component={Contact}/>
        </Switch>
        <Footer />
      </div>
  </Router>
  );
}

export default App;
